package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

public class PriceRepositoryTest {

    private PriceRepository priceRepository = new PriceRepository();

    @Test
    public void getPricesForItems_shouldReturnPrices() throws ItemNotFoundException {
        List<String> items = new ArrayList<>();
        items.add("B");
        items.add("C");

        ImmutableMap<String, Integer> prices = priceRepository.getPricesForItems(items);

        assert(prices.keySet().contains("B"));
        assert(prices.keySet().contains("C"));
        assertThat(prices.keySet().size(), equalTo(2));
        assertThat(prices.get("B"), notNullValue());
        //no tests for specific prices as that makes the test too brittle
    }

    @Test(expected = ItemNotFoundException.class)
    public void getPricesForItems_shouldThrowForItemNotFound() throws ItemNotFoundException {
        List<String> items = new ArrayList<>();
        items.add("E");

        ImmutableMap<String, Integer> prices = priceRepository.getPricesForItems(items);

    }

}