package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class DealRepositoryTest {

    private DealRepository unitUnderTest = new DealRepository();

    @Test
    public void getDealsForItems_shouldGetDealsForItems() {
        List<String> items = new ArrayList<>();
        items.add("A");
        items.add("B");

        ImmutableMap<String, Deal> deals = unitUnderTest.getDealsForItems(items);

        assert(deals.containsKey("A"));
        assert(deals.containsKey("B"));
        assertThat(deals.keySet().size(), equalTo(2));
    }

    @Test
    public void getDealsForItems_shouldReturnEmptyMapForNoDeals() {
        List<String> items = new ArrayList<>();
        items.add("E");

        ImmutableMap<String, Deal> deals = unitUnderTest.getDealsForItems(items);

        assertThat(deals.keySet().size(), equalTo(0));
    }
}