package com.virtualmackem.codetest;

import org.junit.Test;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class DealTest {

    @Test
    public void price_shouldPriceUsingDeal() {
        Deal deal = new Deal(3, 55);

        int price = deal.price(3, 25);

        assertThat(price, equalTo(55));
    }

    @Test
    public void price_shouldPriceRemainderWithoutDeal() {
        Deal deal = new Deal(3, 55);

        int price = deal.price(5, 25);

        assertThat(price, equalTo(105));
    }

}