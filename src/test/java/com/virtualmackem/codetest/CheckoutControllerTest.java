package com.virtualmackem.codetest;

import com.fasterxml.jackson.databind.ObjectMapper;
import jdk.nashorn.internal.ir.debug.JSONWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@WebMvcTest(CheckoutController.class)
public class CheckoutControllerTest {

    @MockBean
    CheckoutService checkoutService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void checkoutShouldReturnBasketWithPrice() throws Exception {

        String content = "{\"items\":[\"A\",\"B\",\"B\",\"C\"]}";
        MvcResult result = mockMvc.perform(post("/checkout/price")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(content))
                .andReturn();

        String expected = "{\"items\":[\"A\",\"B\",\"B\",\"C\"],\"priceInPence\":0}";
        JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
    }
}