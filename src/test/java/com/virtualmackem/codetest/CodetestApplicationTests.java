package com.virtualmackem.codetest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class CodetestApplicationTests {

	@Autowired
	private MockMvc mockMvc;

	@Test
	public void contextLoads() {
	}

	// This test is brittle as it relies on the prices in PriceRepository,
	// but it demonstrates that the pricing works
	@Test
	public void checkoutShouldReturnBasketWithPrice() throws Exception {

		String content = "{\"items\":[\"A\",\"B\",\"B\",\"C\"]}";
		MvcResult result = mockMvc.perform(post("/checkout/price")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content))
				.andReturn();

		String expected = "{\"items\":[\"A\",\"B\",\"B\",\"C\"],\"priceInPence\":115}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}

	@Test
	public void checkoutShouldReturnErrorMessageWhen() throws Exception {

		String content = "{\"items\":[\"A\",\"B\",\"B\",\"C\",\"BAD\"]}";
		MvcResult result = mockMvc.perform(post("/checkout/price")
				.contentType(MediaType.APPLICATION_JSON_UTF8)
				.content(content))
				.andReturn();

		assertThat(result.getResponse().getStatus(), equalTo(404));
		String expected = "{\"items\":[\"A\",\"B\",\"B\",\"C\",\"BAD\"],\"priceInPence\":0,\"errorMessage\":\"Item BAD not found\"}";
		JSONAssert.assertEquals(expected, result.getResponse().getContentAsString(), false);
	}
}
