package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.*;

public class CheckoutTest {

    @Test
    public void calculatePrice_shouldReturnAPrice() {
        ImmutableMap<String, Integer> immutablePrices = givenSomePrices();
        ImmutableMap<String, Deal> deals = ImmutableMap.<String, Deal>builder().build();
        Checkout checkout = new Checkout(immutablePrices, deals);
        Basket basket = givenABasketWithOneAAndTwoBs();

        int price = checkout.calculatePrice(basket);

        assertThat(price, equalTo(50));
    }

    private Basket givenABasketWithOneAAndTwoBs() {
        Basket basket = new Basket();
        ArrayList<String> items = new ArrayList<>(3);
        items.add("B");
        items.add("A");
        items.add("B");
        basket.setItems(items);
        return basket;
    }

    private ImmutableMap<String, Integer> givenSomePrices() {
        HashMap<String, Integer> prices = new HashMap<>();
        prices.put("A", 10);
        prices.put("B", 20);
        return ImmutableMap.copyOf(prices);
    }

}