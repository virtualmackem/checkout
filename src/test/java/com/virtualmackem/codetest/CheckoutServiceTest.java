package com.virtualmackem.codetest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CheckoutServiceTest {

    @Mock
    private CheckoutFactory checkoutFactory;

    @Mock
    private PriceRepository priceRepository;

    @Mock
    private DealRepository dealRepository;

    @Mock
    Checkout checkout;

    @InjectMocks
    private CheckoutService unitUnderTest;

    @Before
    public void setup() {
        when(checkoutFactory.getCheckout(any(), any())).thenReturn(checkout);
    }

    @Test
    public void calculatePrice_shouldCreateACheckoutAndDelegate() throws ItemNotFoundException {
        Basket basket = givenABasket();

        int price = unitUnderTest.calculatePrice(basket);

        verify(checkoutFactory, times(1)).getCheckout(any(), any());
        verify(checkout, times(1)).calculatePrice(basket);
    }

    private Basket givenABasket() {
        Basket basket = new Basket();
        List<String> items = new ArrayList<>(2);
        items.add("A");
        items.add("B");
        basket.setItems(items);
        return basket;
    }

}