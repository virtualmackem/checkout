package com.virtualmackem.codetest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.*;

public class BasketTest {

    @Test
    public void basket_shouldSerializeToJson() throws IOException, JSONException {

        String content = "{\"items\":[\"A\",\"B\",\"B\",\"C\"],\"priceInPence\":0,\"errorMessage\":\"test error\"}";

        ObjectMapper mapper = new ObjectMapper();
        Basket basket = mapper.readValue(content, Basket.class);
        String transformed = mapper.writeValueAsString(basket);
        JSONAssert.assertEquals(content, transformed, false);
    }

    @Test
    public void getItemTotals_shouldReturnTotals() {
        Basket basket = new Basket();
        List<String> items = new ArrayList<>();
        items.add("A");
        items.add("B");
        items.add("A");
        items.add("C");
        basket.setItems(items);

        ImmutableMap<String, Integer> totals = basket.getItemTotals();

        assertThat(totals.get("A"), equalTo(2));
        assertThat(totals.get("B"), equalTo(1));
        assertThat(totals.get("C"), equalTo(1));
        assertThat(totals.keySet().size(), equalTo(3));
    }

    @Test
    public void getItemTotals_shouldReturnEmptyHashmap() {
        Basket basket = new Basket();

        ImmutableMap<String, Integer> totals = basket.getItemTotals();

        assertThat(totals.keySet().size(), equalTo(0));
    }
}