package com.virtualmackem.codetest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/checkout")
public class CheckoutController {

    @Autowired
    private CheckoutService checkoutService;

    // CheckoutController is stateless. Each call takes a basket of items and prices them, then returns the basket
    // together with the price. Errors return a 404 with the basket info and an error message so that the error can be
    // investigated at the client.
    //
    // Making CheckoutController stateless allows horizontal scaling and reduces complexity at the server (as the server
    // doesn't need to keep track of the basket). This, of course, has the consequences that the client is more complex
    // and that message sizes are larger. However, given that baskets are unlikely to be very large this is unlikely to
    // be a problem. Sending the entire basket back in response is also a little wasteful but will be invaluable when
    // problems arise. A different decision might be necessary if message sizes were to become an issue.
    @RequestMapping(value = "/price", method = RequestMethod.POST)
    @ResponseBody public ResponseEntity priceBasket(@RequestBody Basket basket) {
        try {
            int priceInPence = checkoutService.calculatePrice(basket);
            basket.setPriceInPence(priceInPence);
            return ResponseEntity.ok(basket);
        } catch (ItemNotFoundException e) {

            basket.setErrorMessage("Item " + e.getItem() + " not found");
            return ResponseEntity.status(404).body(basket);
        }

    }

}
