package com.virtualmackem.codetest;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

// Basket objects are created from json by Jackson, which has influenced
// the design of this class. Final fields and fewer public methods would
// be preferable
public class Basket {

    private List<String> items = new ArrayList<>();
    private int priceInPence;
    private String errorMessage;

    public List<String> getItems() {
        return Collections.unmodifiableList(items);
    }

    public void setItems(List<String> items) {
        Preconditions.checkNotNull(items);
        this.items = items;
    }

    public int getPriceInPence() {
        return priceInPence;
    }

    public void setPriceInPence(int priceInPence) {
        this.priceInPence = priceInPence;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() { return errorMessage; }

    @JsonIgnore
    public ImmutableMap<String, Integer> getItemTotals() {
        HashMap<String, Integer> totals = new HashMap<>();
        for (String item : items) {
            if (totals.containsKey(item)) {
                totals.put(item, totals.get(item)+1);
            } else {
                totals.put(item, 1);
            }
        }
        return ImmutableMap.copyOf(totals);
    }
}
