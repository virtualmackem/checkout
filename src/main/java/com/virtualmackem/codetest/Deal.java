package com.virtualmackem.codetest;

class Deal {
    private final int multiple;
    private final int priceInPence;

    Deal(int multiple, int priceInPence) {
        this.multiple = multiple;
        this.priceInPence = priceInPence;
    }

    int price(Integer quantity, Integer defaultPrice) {
        int price = 0;
        while (quantity >= multiple) {
            price += priceInPence;
            quantity -= multiple;
        }
        price += quantity * defaultPrice;
        return price;
    }
}
