package com.virtualmackem.codetest;

class ItemNotFoundException extends Exception {

    private final String item;

    ItemNotFoundException(String item) {
        this.item = item;
    }

    String getItem() {
        return item;
    }
}
