package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
class DealRepository {

    private HashMap<String, Deal> deals = new HashMap<>();

    // This repository would normally be a connector to a database where the deals are stored
    // This implementation is for test purposes only
    DealRepository() {
        deals.put("A", new Deal(3, 130));
        deals.put("B", new Deal(2, 45));
    }

    ImmutableMap<String, Deal> getDealsForItems(List<String> items) {

        HashMap<String, Deal> filteredDeals = new HashMap<>();
        for (String item: items) {
            if (deals.containsKey(item)) {
                filteredDeals.put(item, deals.get(item));
            }
        }
        return ImmutableMap.copyOf(filteredDeals);
    }
}
