package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
class CheckoutService {

    @Autowired
    private PriceRepository priceRepository;

    @Autowired
    private DealRepository dealRepository;

    @Autowired
    private CheckoutFactory checkoutFactory;

    int calculatePrice(Basket basket) throws ItemNotFoundException {
        ImmutableMap<String, Integer> prices = priceRepository.getPricesForItems(basket.getItems());
        ImmutableMap<String, Deal> deals = dealRepository.getDealsForItems(basket.getItems());
        Checkout checkout = checkoutFactory.getCheckout(prices, deals);
        return checkout.calculatePrice(basket);
    }
}
