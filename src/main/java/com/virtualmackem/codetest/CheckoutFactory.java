package com.virtualmackem.codetest;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

@Component
class CheckoutFactory {

    // Not adding tests for this method as it doesn't do enough to justify them
    // An @Autowired factory is preferable to a new in the main code as it makes
    // the code more testable
    Checkout getCheckout(ImmutableMap<String, Integer> prices, ImmutableMap<String, Deal> deals) {
        Preconditions.checkNotNull(prices);
        return new Checkout(prices, deals);
    }
}
