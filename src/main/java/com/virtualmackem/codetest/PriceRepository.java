package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
class PriceRepository {

    private final HashMap<String, Integer> prices;

    PriceRepository() {
        prices = new HashMap<>();
        prices.put("A", 50);
        prices.put("B", 30);
        prices.put("C", 20);
        prices.put("D", 15);
    }

    // Provides prices for a given set of items. In a real system this would
    // be your link to a pricing database. We provide prices only for the
    // given items as we might have a lot of prices in the database
    ImmutableMap<String, Integer> getPricesForItems(List<String> items) throws ItemNotFoundException {
        HashMap<String, Integer> filtered = new HashMap<>();
        for (String item : items) {
            if (!prices.containsKey(item)) {
                throw new ItemNotFoundException(item);
            }
            filtered.put(item, prices.get(item));
        }
        return ImmutableMap.copyOf(filtered);
    }
}
