package com.virtualmackem.codetest;

import com.google.common.collect.ImmutableMap;

class Checkout {

    private final ImmutableMap<String, Integer> prices;
    private final ImmutableMap<String, Deal> deals;

    Checkout(ImmutableMap<String, Integer> prices, ImmutableMap<String, Deal> deals) {
        this.prices = prices;
        this.deals = deals;
    }

    int calculatePrice(Basket basket) {
        int price = 0;
        ImmutableMap<String, Integer> itemTotals = basket.getItemTotals();
        for (String item : itemTotals.keySet()) {
            Integer defaultPrice = prices.get(item);
            Integer quantity = itemTotals.get(item);
            if (deals.containsKey(item)) {
                Deal deal = deals.get(item);
                price += deal.price(quantity, defaultPrice);
            } else {
                price += quantity * defaultPrice;
            }
        }
        return price;
    }
}
